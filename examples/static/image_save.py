import sketching


sketch = sketching.Sketch2DStatic(500, 500)

sketch.clear('#707070')
sketch.clear_stroke()
sketch.set_ellipse_mode('radius')
sketch.set_fill('#A6CEE350')

sketch.draw_ellipse(250, 250, 100, 100)
sketch.draw_ellipse(150, 250, 50, 50)
sketch.draw_ellipse(350, 250, 50, 50)

sketch.save_image('image_save.png')
