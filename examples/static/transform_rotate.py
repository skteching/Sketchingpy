import sketching


sketch = sketching.Sketch2DStatic(500, 500)

sketch.clear('#505050')

sketch.clear_stroke()
sketch.set_ellipse_mode('radius')
sketch.set_angle_mode('degrees')

sketch.set_fill('#A6CEE350')
sketch.draw_ellipse(250, 0, 50, 50)

sketch.set_fill('#1F78B450')
sketch.rotate(45)
sketch.draw_ellipse(250, 0, 50, 50)

sketch.set_fill('#B2DF8A50')
sketch.rotate(45)
sketch.draw_ellipse(250, 0, 50, 50)

sketch.save_image('transform_rotate.png')