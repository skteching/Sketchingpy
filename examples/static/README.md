Static Examples
================================================================================
Static rendering examples for the `Sketching.py` library which write to images without requiring the need to show a canvas or window.
