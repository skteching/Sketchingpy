import sketching


sketch = sketching.Sketch2DStatic(500, 500)

sketch.clear('#505050')
sketch.set_stroke_weight(3)

sketch.set_fill('#33A02C')
sketch.set_stroke('#333333')
sketch.draw_ellipse(100, 250, 20, 20)

sketch.clear_fill()
sketch.draw_ellipse(200, 250, 20, 20)

sketch.set_fill('#33A02C50')
sketch.draw_ellipse(300, 250, 20, 20)

sketch.clear_stroke()
sketch.draw_ellipse(400, 250, 20, 20)

sketch.save_image('basics_styling.png')