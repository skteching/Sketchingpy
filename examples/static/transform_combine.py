import sketching


sketch = sketching.Sketch2DStatic(500, 500)

sketch.clear('#505050')

sketch.clear_stroke()
sketch.set_ellipse_mode('radius')
sketch.set_angle_mode('degrees')

sketch.set_fill('#A6CEE350')
sketch.translate(250, 250)
sketch.draw_arc(0, 0, 50, 50, 0, 90)

sketch.rotate(90)
sketch.set_fill('#1F78B450')
sketch.draw_arc(0, 0, 50, 50, 0, 90)

sketch.scale(2)
sketch.set_fill('#B2DF8A50')
sketch.draw_arc(0, 0, 50, 50, 0, 90)

sketch.translate(0, 50)
sketch.set_fill('#33A02C50')
sketch.draw_arc(0, 0, 50, 50, 0, 90)

sketch.save_image('transform_combine.png')