import sketching


records = {
    'a': 1,
    'b': 2,
    'c': 3,
    'd': 4
}

sketch = sketching.Sketch2D(500, 500)
sketch.get_data_layer().write_json(records, 'test.json')
