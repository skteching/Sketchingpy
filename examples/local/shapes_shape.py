import sketching


sketch = sketching.Sketch2D(500, 500)

sketch.clear('#505050')

sketch.clear_fill()
sketch.set_stroke('#A6CEE350')
sketch.set_stroke_weight(5)
shape_outer = sketch.start_shape(250, 133)
shape_outer.add_line_to(387, 371)
shape_outer.add_line_to(114, 371)
shape_outer.end()
sketch.draw_shape(shape_outer)

sketch.set_fill('#1F78B450')
sketch.clear_stroke()
shape_inner = sketch.start_shape(233, 190)
shape_inner.add_line_to(419, 190)
shape_inner.add_line_to(325, 356)
shape_inner.close()
sketch.draw_shape(shape_inner)

sketch.show()