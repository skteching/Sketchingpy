import sketching


sketch = sketching.Sketch2D(500, 500)

sketch.clear('#505050')

sketch.set_text_font('./IBMPlexMono-Regular.ttf', 30)
sketch.set_text_align('center', 'center')

sketch.clear_fill()
sketch.set_stroke('#A6CEE3A0')
sketch.set_stroke_weight(1)
sketch.draw_text(250, 150, 'Sketching')

sketch.set_fill('#B2DF8AA0')
sketch.clear_stroke()
sketch.draw_text(250, 250, 'Sketching')

sketch.set_fill('#B2DF8A')
sketch.set_stroke('#A6CEE3A0')
sketch.set_stroke_weight(1)
sketch.draw_text(250, 350, 'Sketching')

sketch.show()