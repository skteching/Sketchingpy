import sketching
import sketching.const


sketch = sketching.Sketch2D(500, 500)

sketch.set_text_font('./IBMPlexMono-Regular.ttf', 20)
sketch.set_text_align('center', 'center')
sketch.clear_stroke()

mouse = sketch.get_mouse()
press_states = {
    sketching.const.MOUSE_LEFT_BUTTON: False,
    sketching.const.MOUSE_RIGHT_BUTTON: False
}

def handle_press(button):
    press_states[button.get_name()] = True

mouse.on_press(handle_press)

def draw():
    pressed_list = list(mouse.get_buttons_pressed())
    no_keys_pressed = len(pressed_list) == 0
    if no_keys_pressed:
        currently_pressed_str = 'None'
    else:
        pressed_list_str = [x.get_name() for x in pressed_list]
        currently_pressed_str = ', '.join(pressed_list_str)

    if press_states[sketching.const.MOUSE_LEFT_BUTTON]:
        left_str = 'Left press seen.'
    else:
        left_str = 'Waiting for left press.'

    if press_states[sketching.const.MOUSE_RIGHT_BUTTON]:
        right_str = 'Right press seen.'
    else:
        right_str = 'Waiting for right.'

    sketch.clear('#505050')

    sketch.set_fill('#A6CEE3')
    sketch.draw_text(250, 100, left_str)

    sketch.set_fill('#A6CEE3')
    sketch.draw_text(250, 200, right_str)

    sketch.set_fill('#B2DF8A')
    sketch.draw_text(250, 300, 'Currently pressed: ' + currently_pressed_str)

    sketch.set_fill('#B2DF8A')
    coordinates = (mouse.get_x(), mouse.get_y())
    sketch.draw_text(250, 400, 'Currently position: %d, %d' % coordinates)

sketch.on_step(draw)
sketch.show()