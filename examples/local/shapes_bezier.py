import sketching


sketch = sketching.Sketch2D(500, 500)

sketch.clear('#505050')

sketch.clear_fill()
sketch.set_stroke('#A6CEE350')
sketch.set_stroke_weight(10)
shape_top = sketch.start_shape(150, 200)
shape_top.add_bezier_to(200, 150, 250, 300, 350, 250)
shape_top.end()
sketch.draw_shape(shape_top)

sketch.set_fill('#1F78B450')
sketch.clear_stroke()
shape_bottom = sketch.start_shape(150, 250)
shape_bottom.add_bezier_to(200, 200, 250, 350, 350, 300)
shape_bottom.add_line_to(350, 400)
shape_bottom.add_line_to(150, 400)
shape_bottom.close()
sketch.draw_shape(shape_bottom)

sketch.show()