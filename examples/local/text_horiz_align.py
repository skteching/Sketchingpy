import sketching


sketch = sketching.Sketch2D(500, 500)

sketch.clear('#505050')

sketch.set_stroke('#F0F0F050')
sketch.draw_line(250, 50, 250, 450)

sketch.set_text_font('./IBMPlexMono-Regular.ttf', 20)
sketch.clear_stroke()

sketch.set_fill('#33A02CA0')
sketch.set_text_align('left')
sketch.draw_text(250, 150, 'Sketching')

sketch.set_fill('#A6CEE3A0')
sketch.set_text_align('center')
sketch.draw_text(250, 250, 'Sketching')

sketch.set_fill('#B2DF8AA0')
sketch.set_text_align('right')
sketch.draw_text(250, 350, 'Sketching')

sketch.show()