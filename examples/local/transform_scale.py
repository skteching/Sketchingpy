import sketching


sketch = sketching.Sketch2D(500, 500)

sketch.clear('#505050')

sketch.clear_stroke()
sketch.set_ellipse_mode('radius')
sketch.set_angle_mode('degrees')

sketch.set_fill('#A6CEE350')
sketch.draw_ellipse(100, 100, 50, 50)

sketch.set_fill('#1F78B450')
sketch.scale(1.5)
sketch.draw_ellipse(100, 100, 50, 50)

sketch.set_fill('#B2DF8A50')
sketch.scale(2)
sketch.draw_ellipse(100, 100, 50, 50)

sketch.show()