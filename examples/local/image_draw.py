import sketching


sketch = sketching.Sketch2D(500, 500)

image = sketch.load_image('./reference.png')
image.resize(image.get_width() / 2, image.get_height() / 2)

def draw():
    sketch.clear('#707070')

    mouse = sketch.get_mouse()

    sketch.clear_stroke()
    sketch.set_ellipse_mode('radius')
    sketch.set_fill('#A6CEE350')
    sketch.draw_ellipse(250, 250, 100, 100)

    sketch.set_image_mode('center')
    sketch.draw_image(mouse.get_x(), mouse.get_y(), image)

sketch.on_step(draw)

sketch.show()